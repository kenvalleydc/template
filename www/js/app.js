
// admob
document.addEventListener("deviceready", function () {
    var admobid = {};
    if (/(android)/i.test(navigator.userAgent)) {
        admobid = { // for Android
            banner: 'ca-app-pub-1607001084033547/2856711729',
            interstitial: 'ca-app-pub-1607001084033547/5828367721'
        };
    } else if (/(ipod|iphone|ipad)/i.test(navigator.userAgent)) {
        admobid = { // for iOS
            banner: 'ca-app-pub-1607001084033547/5670577322',
            interstitial: 'ca-app-pub-1607001084033547/4072432920'
        };
    } else {
        admobid = { // for Windows Phone
            banner: '',
            interstitial: ''
        };
    }

    if(_lite_version) {
        if (!window.AdMob) {
            alert('admob plugin not ready');
            return;
        }

        window.AdMob.createBanner({
            adId: admobid.banner,
            position: window.AdMob.AD_POSITION.BOTTOM_CENTER,
            autoShow: true
        });

        window.AdMob.prepareInterstitial({adId: admobid.interstitial, autoShow: false});
        window.AdMob.showInterstitial();
    }
}, false);

// Ionic App
angular.module('default', ['ionic'])

    .run(function ($ionicPlatform) {
        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);
            }
            if (window.StatusBar) {
                StatusBar.styleDefault();
            }
        });
    })

    .controller('AppCtrl', function ($scope, $ionicModal, $ionicGesture) {

        $scope.onSideMenuSwipe = function ($event) {
            if ($event.gesture.direction == 'right') {
                var startedFrom = $event.gesture.startEvent.center;
                if (startedFrom.pageX <= window.innerWidth / 3) {
                    $scope.sideMenu.show();
                }
            } else if ($event.gesture.direction == 'left') {
                if ($scope.sideMenu.isShown()) {
                    $scope.sideMenu.hide();
                }
            }
        };

        $scope.checkIos = function(){
            return (/(ipod|iphone|ipad)/i.test(navigator.userAgent));
        };

        $ionicModal.fromTemplateUrl('templates/menu.html', {
            animation: 'slide-in-left',
            scope: $scope
        }).then(function (modal) {
            // Need to manually link ModalView.modalEl since it wasn't able to find a .modal element.
            modal.modalEl = modal.el.querySelector('.menu');

            modal.$el.addClass('sidemenu-modal');

            // Handle swiping the backdrop to close.
            var gesture = $ionicGesture.on('swipe', $scope.onSideMenuSwipe, modal.$el);
            $scope.$on('$destroy', function () {
                $ionicGesture.off(gesture, 'swipe', $scope.onSideMenuSwipe);
            });

            // Modify some of the modal's methods.
            modal._hide = modal.hide;
            modal._show = modal.show;
            modal.show = function () {
                document.body.classList.add('menu-open');
                this._show();
            };
            modal.hide = function () {
                this._hide();
                document.body.classList.remove('menu-open');
            };
            modal.toggle = function () {
                if (this.isShown()) {
                    this.hide();
                } else {
                    this.show();
                }
            };

            $scope.sideMenu = modal;
        });
    })

    .config(function ($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('main', {
                url: "",
                abstract: true,
                templateUrl: "templates/main.html",
                controller: "AppCtrl"
            })
            .state('main.home', {
                url: "/home",
                views: {
                    'menuContent' :{
                        templateUrl: "templates/home.html"
                    }
                }
            })

        $urlRouterProvider.otherwise("/home");
    })